stages:
    - build
    - test
    - tag
    - deploy
    - docs

build:
  image: docker:latest
  stage: build
  interruptible: true
  services:
    - docker:dind
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"

test:
  image: "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
  stage: test
  dependencies:
    - build
  interruptible: true
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    DEPLOY: "test"
  cache:
    paths:
      - .cache/pip
      - env/
  script:
    - /app/venv/bin/pip install pytest-cov pylint
    - pytest --cov=lvfs --cov=plugins --cov=pkgversion --cov=infparser --cov-fail-under=60

tag_production:
  stage: tag
  image: docker:latest
  services:
    - docker:dind
  interruptible: true
  when: manual
  dependencies:
    - test
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker pull "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA" "$CI_REGISTRY_IMAGE:latest"
    - docker push "$CI_REGISTRY_IMAGE:latest"
  only:
    - master

tag_qa:
  stage: tag
  image: docker:latest
  services:
    - docker:dind
  interruptible: true
  when: manual
  dependencies:
    - test
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker pull "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA" "$CI_REGISTRY_IMAGE:staging"
    - docker push "$CI_REGISTRY_IMAGE:staging"
  only:
    - staging

application_production:
  stage: deploy
  image: python:latest
  interruptible: true
  dependencies:
    - tag_production
  when: manual
  script:
    - pip install awscli
    - aws ecs update-service --cluster "$FARGATE_PROD_CLUSTER" --service "$FARGATE_PROD_WEB_SERVICE" --force-new-deployment --region us-west-2
  only:
    - master

metadata_production:
  stage: deploy
  image: python:latest
  interruptible: true
  when: manual
  dependencies:
  - tag_production
  script:
    - pip install awscli
    - aws ecs update-service --cluster "$FARGATE_PROD_CLUSTER" --service "$FARGATE_PROD_METADATA_SERVICE" --force-new-deployment --region us-west-2
  only:
    - master

beat_production:
  stage: deploy
  image: python:latest
  interruptible: true
  when: manual
  dependencies:
    - tag_production
  script:
    - pip install awscli
    - aws ecs update-service --cluster "$FARGATE_PROD_CLUSTER" --service "$FARGATE_PROD_BEAT_SERVICE" --force-new-deployment --region us-west-2
  only:
    - master

application_qa:
  stage: deploy
  image: python:latest
  interruptible: true
  dependencies:
    - tag_qa
  when: manual
  script:
    - pip install awscli
    - aws ecs update-service --cluster "$FARGATE_QA_CLUSTER" --service "$FARGATE_QA_WEB_SERVICE" --force-new-deployment --region us-west-2
  only:
    - staging

metadata_qa:
  stage: deploy
  image: python:latest
  interruptible: true
  when: manual
  dependencies:
  - tag_qa
  script:
    - pip install awscli
    - aws ecs update-service --cluster "$FARGATE_QA_CLUSTER" --service "$FARGATE_QA_METADATA_SERVICE" --force-new-deployment --region us-west-2
  only:
    - staging

beat_qa:
  stage: deploy
  image: python:latest
  interruptible: true
  when: manual
  dependencies:
  - tag_qa
  script:
    - pip install awscli
    - aws ecs update-service --cluster "$FARGATE_QA_CLUSTER" --service "$FARGATE_QA_BEAT_SERVICE" --force-new-deployment --region us-west-2
  only:
    - staging

docs:
  image: alpine:latest
  stage: docs
  interruptible: true
  allow_failure: true
  dependencies:
    - application_production
  variables:
    TOKEN: "$RTD_TOKEN"
  script:
    - apk --no-cache add curl
    - ash build-docs.sh
  only:
    - master
