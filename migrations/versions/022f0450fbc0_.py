"""

Revision ID: 022f0450fbc0
Revises: 8a8e71983bb3
Create Date: 2021-01-25 10:23:41.223450

"""

# revision identifiers, used by Alembic.
revision = '022f0450fbc0'
down_revision = '8a8e71983bb3'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('firmware', sa.Column('ipfs', sa.Text(), nullable=True))


def downgrade():
    op.drop_column('firmware', 'ipfs')
