"""

Revision ID: cac290ed7074
Revises: 8dc3e79088f7
Create Date: 2021-09-27 19:50:32.971910

"""

# revision identifiers, used by Alembic.
revision = "cac290ed7074"
down_revision = "8dc3e79088f7"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "uploads",
        sa.Column("upload_id", sa.Integer(), nullable=False),
        sa.Column("vendor_id", sa.Integer(), nullable=False),
        sa.Column("firmware_revision_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("addr", sa.Text(), nullable=False),
        sa.Column("target", sa.Text(), nullable=False),
        sa.Column("auto_delete", sa.Boolean(), nullable=True),
        sa.Column("done", sa.Boolean(), nullable=True),
        sa.Column("status", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(
            ["firmware_revision_id"],
            ["firmware_revisions.firmware_revision_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("upload_id"),
    )
    op.alter_column(
        "firmware_revisions", "firmware_id", existing_type=sa.INTEGER(), nullable=True
    )


def downgrade():
    op.drop_table("uploads")
    op.alter_column(
        "firmware_revisions", "firmware_id", existing_type=sa.INTEGER(), nullable=False
    )
