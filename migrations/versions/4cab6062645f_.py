"""

Revision ID: 4cab6062645f
Revises: 910d0804cd4d
Create Date: 2021-05-04 20:55:45.447671

"""

# revision identifiers, used by Alembic.
revision = '4cab6062645f'
down_revision = '910d0804cd4d'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_index('ix_firmware_checksum_upload_sha1', table_name='firmware')
    op.create_index(op.f('ix_firmware_checksum_upload_sha1'), 'firmware', ['checksum_upload_sha1'], unique=True)
    op.create_index(op.f('ix_firmware_filename'), 'firmware', ['filename'], unique=True)


def downgrade():
    op.drop_index(op.f('ix_firmware_filename'), table_name='firmware')
    op.drop_index(op.f('ix_firmware_checksum_upload_sha1'), table_name='firmware')
    op.create_index('ix_firmware_checksum_upload_sha1', 'firmware', ['checksum_upload_sha1'], unique=False)
