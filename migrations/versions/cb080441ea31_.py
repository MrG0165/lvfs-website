"""

Revision ID: cb080441ea31
Revises: c8ea83754673
Create Date: 2021-06-30 12:15:56.437691

"""

# revision identifiers, used by Alembic.
revision = "cb080441ea31"
down_revision = "c8ea83754673"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index(
        op.f("ix_firmware_events_remote_id"),
        "firmware_events",
        ["remote_id"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_firmware_events_remote_id"), table_name="firmware_events")
