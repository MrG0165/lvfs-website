"""

Revision ID: ba60b35f0cd2
Revises: 1e3ea6e2d3d0
Create Date: 2022-04-25 11:44:02.185337

"""

# revision identifiers, used by Alembic.
revision = "ba60b35f0cd2"
down_revision = "1e3ea6e2d3d0"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_unique_constraint(
        "uq_md_kind_locale",
        "component_translations",
        ["component_id", "kind", "locale"],
    )


def downgrade():
    op.drop_constraint("uq_md_kind_locale", "component_translations", type_="unique")
