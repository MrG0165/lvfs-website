"""

Revision ID: 75667909c415
Revises: 03707c1bdc5a
Create Date: 2021-11-19 09:01:19.584763

"""

# revision identifiers, used by Alembic.
revision = "75667909c415"
down_revision = "03707c1bdc5a"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("uploads", sa.Column("started_ts", sa.DateTime(), nullable=True))
    op.add_column("uploads", sa.Column("ended_ts", sa.DateTime(), nullable=True))


def downgrade():
    op.drop_column("uploads", "ended_ts")
    op.drop_column("uploads", "started_ts")
