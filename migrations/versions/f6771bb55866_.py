"""

Revision ID: f6771bb55866
Revises: ba60b35f0cd2
Create Date: 2022-04-26 13:12:29.192480

"""

# revision identifiers, used by Alembic.
revision = "f6771bb55866"
down_revision = "ba60b35f0cd2"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "vendor_prodcert",
        sa.Column("vendor_prodcert_id", sa.Integer(), nullable=False),
        sa.Column("vendor_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("visible", sa.Boolean(), nullable=True),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("summary", sa.Text(), nullable=True),
        sa.Column("url", sa.Text(), nullable=True),
        sa.Column("signature_kind", sa.Integer(), nullable=True),
        sa.Column("signature_details", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("vendor_prodcert_id"),
    )
    op.create_index(
        op.f("ix_vendor_prodcert_vendor_id"),
        "vendor_prodcert",
        ["vendor_id"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_vendor_prodcert_vendor_id"), table_name="vendor_prodcert")
    op.drop_table("vendor_prodcert")
